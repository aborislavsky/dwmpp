/* See LICENSE file for copyright and license details. */

/* appearance */
//static const char font[]            = "-*-fixed-medium-r-normal-*-14-*-*-*-*-*-iso10646-*";
//static const char font[]            = "-*-terminus-medium-r-*-*-16-*-*-*-*-*-*-*";

#define VERSION "6.2"

static const char font[]            = "-*-*-medium-*-*-*-10-*-*-*-*-*-iso10646-*";
static const char normbordercolor[] = "#444444";
static const char normbgcolor[]     = "#272822";  //"#ea5f9a"; //"#222222";
static const char normfgcolor[]     = "#a6e22e";  //"#000000"; //"#bbbbbb";
static const char selbordercolor[]  = "#a6e22e"; //"#aaaa71";  //"#961e7e";//"#005577";
static const char selbgcolor[]      = "#1e1e1e";  //"#961e7e";//"#005577";
static const char selfgcolor[]      = "#a6e22e";   //"#000000"; //"#eeeeee";
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "medium:size=9" };
static const char dmenufont[]       = "medium:size=9";
static const char col_gray1[]       = "#272822"; /*"#222222";*/
static const char col_gray2[]       = "#a6e22e";//"#444444";
static const char col_gray3[]       = "#a6e22e"; /*"#bbbbbb";*/
static const char col_gray4[]       = "#a6e22e"; /*"#eeeeee";*/
static const char col_cyan[]        = "#255fdc";//"#1e1e1e"; /*"#005577";*/
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };


/*
 To know window class name run the next:
 xprop |awk '
    /^WM_CLASS/{sub(/.* =/, "instance:"); sub(/,/, "\nclass:"); print}
    /^WM_NAME/{sub(/.* =/, "title:"); print}'
*/
static const Rule rules[] = {
	/* class      	    instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     		NULL,   NULL,       0,            0,        -1 },
	{ "firefox",  		NULL,   NULL,       1 << 8,       0,       -1 },
	{ "Pidgin",   		NULL,   NULL,       1 << 7,       0,       -1 },
	{ "Kontact", 		NULL,   NULL,       1 << 7,       0,       -1 },
	{ "Kopete", 		NULL,   NULL,       1 << 7,       0,       -1 },
	{ "Thunderbird", 	NULL,   NULL,       1 << 7,       0,       -1 },
	{ "osmo", 	        NULL,   NULL,       1 << 7,       0,       -1 },
	{ "urxvt",   		NULL,	NULL,       1 << 0,       0,       -1 },
    { "Linsight",       NULL,   NULL,       1 << 2,       0,       -1 },
    { "linsight",       NULL,   NULL,       1 << 2,       0,       -1 },
    { "kontact",        NULL,   NULL,       1 << 7,       0,       -1 },
    { "korganizer",     NULL,   NULL,       1 << 7,       0,       -1 },
    { "chromium-browser-chromium",NULL,NULL,1 << 6,       0,       -1 },
    { "Chromium-browser",NULL,  NULL,       1 << 6,       0,       -1 },
    { "eonoforder",     NULL,   NULL,       1 << 6,       0,       -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

//#include "grid.c"
void grid(Monitor *m);
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "HHH",      grid },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]           = { "dmenu_run", "-fn", font, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]            = { "alacritty", NULL };
static const char *termcmd2[]           = { "urxvt", NULL };
static const char *monitor[]            = {"sh", "/home/barolgin/.config/dwm/monitor.sh", NULL};
static const char *firefox[]            = {"sh", "/home/barolgin/.config/dwm/firefox.sh", NULL};
static const char *firefoxIncognito[]   = {"firefox", "--private-window", NULL};
static const char *firefoxPrivate[]     = {"firefox", "-P", "private", NULL};
static const char *firefoxOzzy[]        = {"firefox", "-P", "ozzy", NULL};
static const char *mailclient[]         = {"thunderbird", NULL};
static const char *leafpad[]            = {"geany", NULL};
static const char *qtcreator[]          = {"qtcreator", NULL};
static const char *printscreen[]        = {"sh", "/home/barolgin/.config/dwm/printscreen.sh", NULL};
static const char *filemanager[]        = {"dbus-launch", "pcmanfm-qt", NULL};
static const char *links[]              = {"links", "-g", "google.com", NULL};
static const char *slock[]              = {"slock", NULL};
static const char *vm_office[]          = {"VirtualBoxVM", "-startvm", "office", NULL};
static const char *wpa_gui[]            = {"wpa_gui", NULL};
static const char *rotateScreenLeft[]   = {"xrandr", "--output", "eDP-1", "--rotate", "left", NULL};
static const char *rotateScreenRight[]  = {"xrandr", "--output", "eDP-1", "--rotate", "right", NULL};
static const char *rotateScreenNormal[] = {"xrandr", "--output", "eDP-1", "--rotate", "normal", NULL};
static const char *rotateScreenInvert[] = {"xrandr", "--output", "eDP-1", "--rotate", "inverted", NULL};
static const char *quitSession[]        = {"sh", "/home/barolgin/.config/dwm/stopDwm.sh", NULL};
static const char *enuk_kb[]            = {"bash", "/opt/bin/layout.sh", NULL};
static const char *eonoforder[]         = {"eonoforder", NULL};
static const char *eonoforder_priv[]    = {"eonoforder", "eonoforder-priv", NULL};
static const char *host5_vnc[]          = {"vncviewer", "-DotWhenNoCursor", "-PasswordFile", "/home/barolgin/.vnc/host5-passwd", "-FullScreen", "host5", NULL};
static const char *ozzy_vnc[]           = {"vncviewer", "-DotWhenNoCursor", "-PasswordFile", "/home/barolgin/.vnc/office-passwd", "-FullScreen", "ozzy:5901", NULL};

/* volume managing */
static const char *volumeUp[]           = {"amixer", "-q", "-D", "pulse", "sset", "Master", "5%+", NULL};
static const char *volumeDown[]         = {"amixer", "-q", "-D", "pulse", "sset", "Master", "5%-", NULL};

/* backlight managing */
static const char *backlightUp[]        = {"xbacklight", "-inc", "5", NULL};
static const char *backlightDown[]      = {"xbacklight", "-dec", "5", NULL};

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
    { MODKEY|Mod4Mask,  XK_q, spawn,          {.v = quitSession } },
	{Mod4Mask,          XK_l, spawn,          {.v = slock } },
    {Mod4Mask,          XK_h, spawn,          {.v = host5_vnc} },
    {Mod4Mask,          XK_g, spawn,          {.v = ozzy_vnc} },
	{Mod4Mask,			XK_p, spawn,          {.v = monitor } },
	{Mod4Mask,			XK_r, spawn,          {.v = dmenucmd } },
	{Mod4Mask,			XK_b, spawn,          {.v = firefox } },
	{Mod4Mask,			XK_n, spawn,          {.v = leafpad } },
	{Mod4Mask,			XK_t, spawn,          {.v = mailclient } },
	{Mod4Mask,			XK_q, spawn,          {.v = qtcreator } },
	{Mod4Mask,			XK_c, spawn,          {.v = termcmd } },
	{Mod4Mask,			XK_x, spawn,          {.v = termcmd2 } },
	{Mod4Mask,			XK_k, spawn,          {.v = enuk_kb } },
	{Mod4Mask,			XK_e, spawn,          {.v = filemanager } },
	{Mod4Mask,			XK_w, spawn,          {.v = wpa_gui } },
	{Mod4Mask,			XK_i, spawn,          {.v = eonoforder_priv } },
	{0,				    0xff61, spawn,        {.v = printscreen } },
	{0,				    0x1008ff11, spawn,    {.v = volumeDown } },
	{0,				    0x1008ff13, spawn,    {.v = volumeUp } },
	{0,				    0x1008ff03, spawn,    {.v = backlightDown } },
	{0,				    0x1008ff02, spawn,    {.v = backlightUp } },
	{Mod4Mask,          XK_o, spawn,          {.v = eonoforder } },
	{Mod4Mask,          XK_Left, spawn,       {.v = rotateScreenLeft } },
	{Mod4Mask,          XK_Right, spawn,      {.v = rotateScreenRight } },
	{Mod4Mask,          XK_Up, spawn,         {.v = rotateScreenNormal } },
	{Mod4Mask,          XK_Down, spawn,       {.v = rotateScreenInvert } },
	{Mod4Mask,          XK_u, spawn,          {.v = enuk_kb } },
    {Mod4Mask,          XK_j, spawn,          {.v = firefoxPrivate } },
    {Mod4Mask,          XK_v, spawn,          {.v = firefoxOzzy } },
    {Mod4Mask,          XK_m, spawn,          {.v = firefoxIncognito } },
};

/*
133, Super_L
0x6b, k
*/

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

